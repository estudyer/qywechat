<?php

namespace Estudyer\Qywx;

/**
 * Defines a few helper methods.
 */
class Helpers
{

    /**
     * 检测一个字符串否为Json字符串
     *
     * @param string $string            
     * @return true/false
     *
     */
    public static function isJson(string $string): bool
    {
        if (str_contains($string, "{")) {
            json_decode($string);
            return (json_last_error() === JSON_ERROR_NONE);
        }

        return false;
    }

    /**
     * 除去数组中的空值和签名参数
     *
     * @param array $para 签名参数组
     * return 去掉空值与签名参数后的新签名参数组
     * @return array
     */
    public static function paraFilter(array $para): array
    {
        $para_filter = array();
        foreach ($para as $key => $val) {
            if (strtolower(trim($key)) === 'sign' || trim($val) === '') continue;
            $para_filter[$key] = $val;
        }
        return $para_filter;
    }

    /**
     * 对数组排序
     *
     * @param array $para 排序前的数组
     * return 排序后的数组
     * @return array
     */
    public static function argSort(array $para): array
    {
        ksort($para, SORT_STRING);
        reset($para);
        return $para;
    }

    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     *
     * @param array $para 需要拼接的数组
     * return 拼接完成以后的字符串
     * @return string
     */
    public static function createLinkstring(array $para): string
    {
        $arg = '';
        foreach ($para as $key => $val) {
            $arg .= $key . '=' . $val . '&';
        }
        // 去掉最后一个&字符
        return substr($arg, 0, strlen($arg) - 1);
    }

    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，并对字符串做urlencode编码
     *
     * @param array $para 需要拼接的数组
     * return 拼接完成以后的字符串
     * @return string
     */
    public static function createLinkstringUrlencode(array $para): string
    {
        $arg = '';
        foreach ($para as $key => $val) {
            $arg .= $key . '=' . rawurlencode($val) . '&';
        }
        // 去掉最后一个&字符
        return substr($arg, 0, strlen($arg) - 1);
    }

    /**
     * 获取随机字符串
     *
     * @param int $length
     * @return string
     */
    public static function createNonceStr(int $length = 16): string
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }

        return $str;
    }

    /**
     * 作用：array转xml
     * @param array $arr
     * @return string
     */
    public static function arrayToXml(array $arr): string
    {
        $xml = '<xml>';
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= '<' . $key . '>' . $val . '</' . $key . '>';
            } else
                $xml .= '<' . $key . '><![CDATA[' . $val . ']]></' . $key . '>';
        }
        $xml .= '</xml>';
        return $xml;
    }

    /**
     * 作用：将xml转为array
     */
    public static function xmlToArray($xml)
    {
        if (function_exists('libxml_disable_entity_loader')) libxml_disable_entity_loader();

        $object = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        return @json_decode(preg_replace('/{}/', '""', @json_encode($object)), 1);
    }
}
