<?php

/**
 * 企业微信客户端总调度器
 */

namespace Estudyer\Qywx;

use Estudyer\Qywx\Manager\{Agent, Appchat, Batch, Card, Checkin, Department, Dial, ExternalContact};
use Exception;
use Estudyer\Qywx\Http\Request;
use Estudyer\Qywx\ThirdParty\MsgCrypt\WXBizMsgCrypt;
use Estudyer\Qywx\Manager\{Ip, Media, Menu, Message, Oa, Reply, Tag, User, MsgAudit};
use Estudyer\Qywx\Manager\{Living, Health, Linkedcorp, Kf, Export, License, Idconvert, Corp};

class Client
{
    private string $_corpId;

    private string $_corpSecret;

    private ?Request $_request = null;

    private $_accessToken = null;

    private string $_from;

    private string $_to;

    /**
     * @param string $corpId
     * @param string $corpSecret
     */
    public function __construct(string $corpId, string $corpSecret)
    {
        $this->_corpId = $corpId;
        $this->_corpSecret = $corpSecret;
    }

    /**
     * @return string
     */
    public function getCorpId(): string
    {
        return $this->_corpId;
    }

    /**
     * @return string
     */
    public function getCorpSecret(): string
    {
        return $this->_corpSecret;
    }

    /**
     * 获取服务端的accessToken
     *
     * @throws Exception
     */
    public function getAccessToken()
    {
        if (empty($this->_accessToken)) throw new Exception('请设定access_token');

        return $this->_accessToken;
    }

    /**
     * 设定服务端的access token
     *
     * @param string $accessToken
     * @return Client
     */
    public function setAccessToken(string $accessToken): static
    {
        $this->_accessToken = $accessToken;

        return $this;
    }

    /**
     * 获取来源用户
     *
     * @return string
     * @throws Exception
     */
    public function getFromUserName(): string
    {
        if (empty($this->_from)) throw new Exception('请设定FromUserName');

        return $this->_from;
    }

    /**
     * 获取目标用户
     *
     * @return string
     * @throws Exception
     */
    public function getToUserName(): string
    {
        if (empty($this->_to)) throw new Exception('请设定ToUserName');

        return $this->_to;
    }

    /**
     * 设定来源和目标用户
     *
     * @param string $fromUserName
     * @param string $toUserName
     * @return Client
     */
    public function setFromAndTo(string $fromUserName, string $toUserName): static
    {
        $this->_from = $toUserName;
        $this->_to = $fromUserName;

        return $this;
    }

    /**
     * 初始化认证的http请求对象
     * @throws Exception
     */
    private function initRequest(): void
    {
        $this->_request = new Request($this->getAccessToken());
    }

    /**
     * 获取请求对象
     *
     * @return Request|null
     * @throws Exception
     */
    public function getRequest(): ?Request
    {
        if (empty($this->_request)) $this->initRequest();

        return $this->_request;
    }

    /**
     * 获取应用管理
     *
     * @return Agent
     */
    public function getAgentManager(): Agent
    {
        return new Agent($this);
    }

    /**
     * 获取发送消息到群聊会话管理器
     *
     * @return Appchat
     */
    public function getAppchatManager(): Appchat
    {
        return new Appchat($this);
    }

    /**
     * 获取批量管理器
     *
     * @return Batch
     */
    public function getBatchManager(): Batch
    {
        return new Batch($this);
    }

    /**
     * 获取卡管理器
     *
     * @return Card
     */
    public function getCardManager(): Card
    {
        return new Card($this);
    }

    /**
     * 获取企业微信打卡应用管理器
     *
     * @return Checkin
     */
    public function getCheckinManager(): Checkin
    {
        return new Checkin($this);
    }


    /**
     * 获取部门管理器
     *
     * @return Department
     */
    public function getDepartmentManager(): Department
    {
        return new Department($this);
    }


    /**
     * 获取企业微信公费电话管理器
     *
     * @return Dial
     */
    public function getDialManager(): Dial
    {
        return new Dial($this);
    }


    /**
     * 获取外部企业的联系人管理器
     *
     * @return ExternalContact
     */
    public function getExternalContactManager(): ExternalContact
    {
        return new ExternalContact($this);
    }


    /**
     * 获取企业微信服务器IP地址管理器
     *
     * @return Ip
     */
    public function getIpManager(): Ip
    {
        return new Ip($this);
    }

    /**
     * 获取互联企业管理器
     *
     * @return Linkedcorp
     */
    public function getLinkedcorpManager(): Linkedcorp
    {
        return new Linkedcorp($this);
    }

    /**
     * 获取素材管理器
     *
     * @return Media
     */
    public function getMediaManager(): Media
    {
        return new Media($this);
    }

    /**
     * 获取菜单管理器
     *
     * @return Menu
     */
    public function getMenuManager(): Menu
    {
        return new Menu($this);
    }

    /**
     * 获取主动发送消息器
     *
     * @return Message
     */
    public function getMessageManager(): Message
    {
        return new Message($this);
    }

    /**
     * 获取企业微信审批应用管理器
     *
     * @return Oa
     */
    public function getOaManager(): Oa
    {
        return new Oa($this);
    }

    /**
     * 获取被动回复发送器
     *
     * @return Reply
     */
    public function getReplyManager(): Reply
    {
        return new Reply($this);
    }

    /**
     * 获取标签管理器
     *
     * @return Tag
     */
    public function getTagManager(): Tag
    {
        return new Tag($this);
    }

    /**
     * 获取成员管理器
     *
     * @return User
     */
    public function getUserManager(): User
    {
        return new User($this);
    }

    /**
     * 获取会话内容存档管理器
     *
     * @return MsgAudit
     */
    public function getMsgAuditManager(): MsgAudit
    {
        return new MsgAudit($this);
    }

    /**
     * 获取企业直播管理器
     *
     * @return Living
     */
    public function getLivingManager(): Living
    {
        return new Living($this);
    }

    /**
     * 获取健康上报管理器
     *
     * @return Health
     */
    public function getHealthManager(): Health
    {
        return new Health($this);
    }

    /**
     * 获取微信客服管理器
     *
     * @return Kf
     */
    public function getKfManager(): Kf
    {
        return new Kf($this);
    }

    /**
     * 获取导出管理器
     *
     * @return Export
     */
    public function getExportManager(): Export
    {
        return new Export($this);
    }

    /**
     * 获取接口调用许可管理器
     *
     * @return License
     */
    public function getLicenseManager(): License
    {
        return new License($this);
    }

    /**
     * 获取ID转换接口管理器
     *
     * @return Idconvert
     */
    public function getIdconvertManager(): Idconvert
    {
        return new Idconvert($this);
    }

    /**
     * 获取Corp管理器
     *
     * @return Corp
     */
    public function getCorpManager(): Corp
    {
        return new Corp($this);
    }

    /**
     * 签名校验
     *
     * @param string $token
     * @param string $encodingAesKey
     * @return array|bool
     */
    public function checkSignature(string $token, string $encodingAesKey = ''): array|bool
    {
        $token = trim($token);
        $signature = isset($_GET['msg_signature']) ? trim($_GET['msg_signature']) : '';
        $timestamp = isset($_GET['timestamp']) ? trim($_GET['timestamp']) : '';
        $nonce = isset($_GET['nonce']) ? trim($_GET['nonce']) : '';
        $echostr = isset($_GET['echostr']) ? trim($_GET['echostr']) : '';

        // 需要返回的明文
        $sEcsReplyEchoStrhoStr = "";
        $wxcpt = new WXBizMsgCrypt($token, $encodingAesKey, $this->getCorpId());
        $errCode = $wxcpt->VerifyURL($signature, $timestamp, $nonce, $echostr, $sEcsReplyEchoStrhoStr);

        if ($errCode == 0) {
            // 验证URL成功，将sEchoStr返回
            return array(
                'replyEchoStr' => $sEcsReplyEchoStrhoStr
            );
        } else {
            return false;
        }
    }

    /**
     * 有效性校验
     * @param string $token
     * @param string $encodingAesKey
     * @throws Exception
     */
    public function verify(string $token, string $encodingAesKey = ''): void
    {
        if (empty($token)) throw new Exception("请设定校验签名所需的token");

        $ret = $this->checkSignature($token, $encodingAesKey);

        if (!empty($ret)) exit($ret['replyEchoStr']);
    }

    /**
     * 标准化处理微信的返回结果
     * @param $rst
     * @return mixed
     */
    public function rst($rst): mixed
    {
        return $rst;
    }
}
